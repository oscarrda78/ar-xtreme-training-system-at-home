﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Vuforia;

public class HeatingMenuController : MonoBehaviour
{
    [SerializeField] private GameObject instructions;
    private IntructionsController intructionsController;
    [SerializeField] private GameObject trainingController;
    [SerializeField] private GameObject selectTargetPanel;
    [SerializeField] private GameObject objectObjGenerate;
    [SerializeField] private Button menu;
    [SerializeField] private Button heating;
    // Start is called before the first frame update
    void Start()
    {
        trainingController.SetActive(false);
        objectObjGenerate.SetActive(false);
        selectTargetPanel.SetActive(false);
        intructionsController = instructions.GetComponent<IntructionsController>();
        heating.onClick.AddListener(delegate { GoHeating(); });
        menu.onClick.AddListener(delegate { GoToScene("Menu"); });
        TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void GoHeating()
    {
        intructionsController.notIsCustom = true;
        intructionsController.file = "heating";
        objectObjGenerate.SetActive(true);
        instructions.SetActive(true);
        this.gameObject.SetActive(false);
    }
        
    private void GoToScene(string otherScene)
    {
        SceneManager.LoadScene(otherScene);
    }
}
