﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinalMenu : MonoBehaviour
{
    [SerializeField] private Button menuBtn;
    [SerializeField] private Button trainingBtn;
    [SerializeField] private Button tryAgainBtn;
    [SerializeField] private FileDataManager fileDataManager;
    
    [SerializeField] private TraningController traningController;
    [SerializeField] private ObjectGenerate objectGenerate;
    [SerializeField] private GameObject instructions;
    [SerializeField] private GameObject progressBar;
    [SerializeField] private Timer timer;
    private ProgressBar progressBarC;
    private string[] exercises;
    private string type;

    [SerializeField] private Text finalScore;
    [SerializeField] private Text hitObjects;
    [SerializeField] private Text dodgedObjects;

    [SerializeField] private GameObject scoreBar;
    private SessionList sessionList;
    
    private float fScore;
    private float hObjects;
    private float dObjects;
    private bool first;
    
    // Start is called before the first frame update
    void Start()
    {
        trainingBtn.onClick.AddListener(delegate { NextSession(); });
        if (gameObject.CompareTag("TrainingController"))
        {
            progressBarC = progressBar.GetComponent<ProgressBar>();
            progressBarC.ResetProgress();
        }
        else
        {
            timer.resetTimer();
        }
        
        scoreBar.SetActive(false);
        progressBar.SetActive(false);
        tryAgainBtn.gameObject.SetActive(false);
        instructions.SetActive(false);
        menuBtn.onClick.AddListener(delegate { GoToScene("Menu");});

        first = true;
        fScore = traningController.score;

        exercises = objectGenerate.exercises.ToArray();
        type = objectGenerate.type;

        hObjects = ((float)traningController.hitObjects/(traningController.obstaclesAmount));
        dObjects = ((float)(traningController.obstaclesAmount-traningController.hitObjects)/(traningController.obstaclesAmount));
        dObjects = dObjects * 100;
        hObjects = hObjects * 100;
        dObjects = (float) (Math.Round((double) dObjects, 2));
        hObjects = (float) (Math.Round((double) hObjects, 2));
        
        finalScore.text = fScore.ToString();
        hitObjects.text = hObjects + "%";
        dodgedObjects.text = dObjects + "%";
        
    }

    // Update is called once per frame
    void Update()
    {
        if (objectGenerate.lastList)
        {
            trainingBtn.gameObject.SetActive(false);
            tryAgainBtn.gameObject.SetActive(true);
            tryAgainBtn.onClick.AddListener(delegate { GoMenu(); });
        }
    }
    
    private void GoMenu()
    {
        if (first && traningController.CompareTag("TrainingController"))
        {
            string date = System.DateTime.Now.Day.ToString() + "/" + System.DateTime.Now.Month.ToString() + "/" +
                           System.DateTime.Now.Year.ToString();
            string hora = System.DateTime.Now.Hour.ToString("00") + ":" + System.DateTime.Now.Minute.ToString("00");
            Session session = new Session(hObjects, dObjects, hora,date,type,exercises,(int) fScore);
            fileDataManager.Save("sessions.txt", session);
            first = false;
        }
        GoToScene("Menu");
    }
    
    
    private void NextSession()
    {
        objectGenerate.Clean();
        traningController.score = 0;
        traningController.dodgedObjects = 0;
        traningController.hitObjects = 0;
        objectGenerate.CreateObstacles(objectGenerate.NextObstacleList());
        instructions.SetActive(true);
        gameObject.SetActive(false);
    }
    
    
    void GoToScene(string otherScene)
    {
        SceneManager.LoadScene(otherScene);
    }
}
