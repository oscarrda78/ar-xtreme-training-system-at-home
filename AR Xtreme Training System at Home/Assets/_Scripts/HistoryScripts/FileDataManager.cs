using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class FileDataManager : MonoBehaviour
{
    SessionList sessionList;
    private List<Session> sessions = new List<Session>();
    public void Save(string file, Session data)
    {
        sessionList = new SessionList();
        if (pathExists("sessions.txt"))
        {
            Load("sessions.txt", sessionList);
            sessions = sessionList.session.ToList();
        }
        sessions.Add(data);
        sessionList = new SessionList(sessions.ToArray());
        string json = JsonUtility.ToJson(sessionList);
        WriteToFile(file, json);
    }
    public void Load(string file, SessionList data)
    {
        string json = ReadFromFile(file);
        JsonUtility.FromJsonOverwrite(json, data);
    }
    public void WriteToFile(string _fileName, string _json)
    {
        string path = GetFilePath(_fileName);
        FileStream fileStream = new FileStream(path, FileMode.Create);
        using (StreamWriter writer = new StreamWriter(fileStream))
        {
            writer.Write(_json);
        }
    }

    public bool pathExists(string _fileName)
    {
        string path = GetFilePath(_fileName);
        if (File.Exists(path))
        {
            return true;
        }

        return false;
    }

    public string ReadFromFile(string _fileName)
    {
        string path = GetFilePath(_fileName);
        if (File.Exists(path))
        {
            using (StreamReader reader = new StreamReader(path))
            {
                string json = reader.ReadToEnd();
                return json;
            }
        }
        else
            print("File Not Found");
        return "";
    }

    public string GetFilePath(string fileName)
    {
        print(Application.persistentDataPath + "/" + fileName);
        return Application.persistentDataPath + "/" + fileName;
    }
}