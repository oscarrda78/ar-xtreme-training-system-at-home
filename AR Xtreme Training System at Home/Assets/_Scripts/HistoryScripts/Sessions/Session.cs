using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Session
{
    public float hitObjects;
    public float dodgedObjects;
    public string hour;
    public string day;
    public string[] exercises;
    public string type;
    public int points;

    public Session(float x_, float y_, string h_, string d_, string t, string[] e, int z_)
    {
        this.hitObjects = x_;
        this.dodgedObjects = y_;
        this.hour = h_;
        this.day = d_;
        this.exercises = e;
        this.type = t;
        this.points = z_;
    }
}