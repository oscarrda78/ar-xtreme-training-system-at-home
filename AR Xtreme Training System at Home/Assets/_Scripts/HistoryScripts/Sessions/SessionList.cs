using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[System.Serializable]
public class SessionList
{
    public Session[] session;

    public Session getSession(int i)
    {
        return session[i];
    }

    public SessionList()
    {
        
    }

    public SessionList(Session[] session_)
    {
        this.session = session_;
    }

    public int getSize()
    {
        return session.Length;
    }

    public void add(Session session_)
    {
        session.Append(session_);
    }
}