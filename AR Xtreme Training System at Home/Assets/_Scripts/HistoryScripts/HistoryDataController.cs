﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HistoryDataController : MonoBehaviour
{
    [SerializeField] private FileDataManager fileDataManager;
    [SerializeField] private Text text;
    [SerializeField] private Button back;
    [SerializeField] private DashboardController dashboard1;
    [SerializeField] private DashboardController dashboard2;
    private Session[] sessions;
    private SessionList sessionList;
    private string exercises;
    private float dodget;
    private float hit;
    
    // Start is called before the first frame update
    void Start()
    {
        GameObject sessionTemplate = transform.GetChild(0).gameObject;
        GameObject g;
        if (fileDataManager.pathExists("sessions.txt"))
        {
            sessionTemplate.SetActive(true);
            text.gameObject.SetActive(false);
            sessionList = new SessionList();
            fileDataManager.Load("sessions.txt", sessionList);
            print(sessionList.getSize());
            sessions = sessionList.session;

            for (int n = 0; n < sessionList.getSize(); n++)
            {
                dodget += sessions[n].dodgedObjects;
                hit += sessions[n].hitObjects;
            }

            dodget = dodget / sessionList.getSize();
            hit = hit / sessionList.getSize();
            dashboard1.fillAmount = dodget;
            dashboard2.fillAmount = hit;
            
            for (int i = 0; i < sessionList.getSize(); i++)
            {
                g = Instantiate(sessionTemplate, transform);
                g.transform.GetChild(0).GetComponent<Text>().text = "Session " + (i + 1);
                g.transform.GetChild(1).GetComponent<Text>().text = "Hit objects: " + sessions[i].hitObjects + "%";
                g.transform.GetChild(2).GetComponent<Text>().text = "Dodged objects: " + sessions[i].dodgedObjects + "%";
                g.transform.GetChild(3).GetComponent<Text>().text = "Day: " + sessions[i].day;
                g.transform.GetChild(4).GetComponent<Text>().text = "Hour: " + sessions[i].hour;
                g.transform.GetChild(5).GetComponent<Text>().text = "Type of Training: " + sessions[i].type;
                for (int k = 0; k < sessions[i].exercises.Length; k++)
                {
                    if (k == 0)
                    {
                        exercises = sessions[i].exercises[k];
                    }
                    else
                    {
                        exercises = String.Concat(exercises, ", ", sessions[i].exercises[k]);
                    }
                }
                g.transform.GetChild(6).GetComponent<Text>().text = "Exercises of Training: " + exercises;
            }
            Destroy(sessionTemplate);
        }
        else
        {
            text.gameObject.SetActive(true);
            sessionTemplate.SetActive(false);
        }
        back.onClick.AddListener(delegate { GoToScene("Menu"); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void GoToScene(string otherScene)
    {
        SceneManager.LoadScene(otherScene);
    }
}
