﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashboardController : MonoBehaviour
{
    public float fillAmount = 0f;
    [SerializeField] private Text progressText;
    [SerializeField] private Image mask;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        progressText.text = (fillAmount).ToString();
        mask.fillAmount = fillAmount/100;
    }
    
}
