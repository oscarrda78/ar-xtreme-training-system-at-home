using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[System.Serializable]
public class ObstacleList
{
    public int id;
    public string name;
    public string description;
    public int video;
    public int repeat;
    public Obstacle[] obstacle;

    public Obstacle getObstacle(int i)
    {
        return obstacle[i];
    }

    public ObstacleList(string name_, string description_, int video_, int repeat_ ,Obstacle[] obstacle_)
    {
        this.name = name_;
        this.description = description_;
        this.video = video_;
        this.repeat = repeat_;
        this.obstacle = obstacle_;
    }

    public int getSize()
    {
        return obstacle.Length;
    }

    public void add(Obstacle obstacle_)
    {
        obstacle.Append(obstacle_);
    }
}