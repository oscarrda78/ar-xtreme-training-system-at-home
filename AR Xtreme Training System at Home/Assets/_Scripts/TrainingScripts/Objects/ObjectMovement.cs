﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectMovement : MonoBehaviour
{
    [SerializeField, Range(1f,100f)] private float velocity = 50.0f;
    [SerializeField] private TraningController traningController;
    private Rigidbody rigidbody;
    private Renderer renderer;
    private bool kicked;

    public bool getKicked()
    {
        return this.kicked;
    }
    
    public int score = 40;
    private void Awake()
    {
        //Instantiate components
        renderer = GetComponent<Renderer>();
        rigidbody = GetComponent<Rigidbody>();
        traningController = GameObject.Find("TrainingController").GetComponent<TraningController>();
    }

    void FixedUpdate()
    {
        if(kicked){
            rigidbody.velocity = Vector2.zero;
        }else{
            rigidbody.AddForce(-transform.forward * velocity * Time.deltaTime); // MRUV
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("PointReference"))
        {
            ChangeColor(Color.green);
            traningController.dodgedObjects++;
            Debug.Log("Esquivados: " + traningController.dodgedObjects);
            traningController.score += score;
            StartCoroutine(DestroyCube());
        }
        if (other.gameObject.CompareTag("PointCameraReference"))
        {
            kicked = true;
            ChangeColor(Color.red);
            traningController.hitObjects++;
            Debug.Log("Golpeados: " + traningController.hitObjects);
            StartCoroutine(DestroyCube());
        }
    }
    IEnumerator DestroyCube(){
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
    void ChangeColor(Color color){
        renderer.material.SetColor("_Color",color);
    }
    
    
}
