using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Obstacle
{
    public int type;
    public float x;
    public float y;
    public float z;

    public Obstacle(int type_, float x_, float y_, float z_)
    {
        this.type = type_;
        this.x = x_;
        this.y = y_;
        this.z = z_;
    }
}