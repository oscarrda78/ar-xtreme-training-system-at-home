﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public class TraningController : MonoBehaviour
{
    [SerializeField] private  SelectTargetController selectTargetController;
    [SerializeField] private ObjectGenerate objectGenerate;
    [SerializeField] private GameObject tutorial;
    [SerializeField] private GameObject progressBar;
    [SerializeField] private GameObject time;
    [SerializeField] private GameObject scoreBar;
    [SerializeField] private Text textTutorial;

    [SerializeField] private GameObject finalMenu;
    private ObstacleList obstacleList;
    private Obstacle obstacleX;
    public string file;
    public string type;
    public string[] exercises;
    
    [SerializeField] private GameObject[] obstacles;

    [Header("Timer")]
    [SerializeField] private GameObject timerObject;
    private Timer timer;
    private int cylinderCount;
    private int latCount;
    private int i;

    //[Header("Difficulty Menu")]
    //[SerializeField] private GameObject dmcObject;
    private DifficultyMenuController dmc;
    
    private int obstacleIndex;
    public int obstaclesAmount;
    public int obstaclesCount;
    public int hitObjects;
    public int dodgedObjects;
    
    public float limitTime;
    private float currentTime;

    public bool playing = false;
    public bool first = false;
    
    
    private Transform targetProperties;
    private Transform initialTargetProperties;
    public float currentTimeToInstantiate = 0f;
    public float startTimeToInstantiate = 1f;
    public float waitTimeToInstantiate = 3f;
    
    private ObjectMovement obj;
    private GameObject obstacle;
    public bool generate = true;
    
    [Header("Validate Data")]
    public int score;
    public bool isTraining;
    public bool isCustom;
    public string sessionName;
    private int objectCount;
    
    [SerializeField] private Vector3 agregate = new Vector3(0, 0.05f, 0);
    
    Quaternion rotation;
    private bool dumm = true;
    
    void Start()
    {
        tutorial.SetActive(false);
        timer = timerObject.GetComponent<Timer>();
        if (gameObject.CompareTag("TrainingController"))
        {
            isTraining = true;
        }
        else
        {
            file = "heating";
            timer.timeScale = 0;            
        }
        
        obstacleList = objectGenerate.obstacleList;
        limitTime = timer.limitTime;
        currentTimeToInstantiate = startTimeToInstantiate;
        //initialTargetProperties = selectTargetController.targetSelected;
    }

    public void setLimitTime(float time)
    {
        limitTime = time;
    }

    public void setObstaclesAmount(int obstacles)
    {
        obstaclesAmount = obstacles;
    }

    
    void Update()
    {
        print(playing);
        print(first);
        if (playing && first)
        {
            score = 0;
            obstaclesCount = 0;
            hitObjects = 0;
            dodgedObjects = 0;
            generate = true;
            progressBar.SetActive(true);
            time.SetActive(true);
            
            scoreBar.SetActive(true);
            first = false;
            objectGenerate.Load();
        }
        print(score);
        if (playing &&  obstaclesCount < 2)
        {
            obstacleX = obstacleList.getObstacle(obstaclesCount);
            obstacleIndex = obstacleX.type;
            agregate = new Vector3(obstacleX.x, obstacleX.y, obstacleX.z);
        }

        if(selectTargetController.isCountDown && dumm){
            targetProperties = selectTargetController.targetSelected;
            rotation = transform.rotation;
            dumm = false;
        }
        if (isTraining)
        {
            obstaclesAmount = obstacleList.getSize();
            InstantiateTrainingObstacles();

        }
        else
        {
            obstaclesAmount = obstacleList.getSize();
            currentTime = timer.timeinSecondsToShow;
            if (generate)
            {
                InstantiateHeatingObstacles();
            }
        }
    }


    void InstantiateTrainingObstacles()
    {
        if (selectTargetController.canMove && obstaclesCount < obstaclesAmount)
        {
            currentTimeToInstantiate -= 1 * Time.deltaTime;
            if (currentTimeToInstantiate <= 0)
            {
                obstaclesCount++;
                switch (obstacleIndex)
                {
                    case 1:
                        obstacle = Instantiate(obstacles[obstacleIndex], targetProperties.position + agregate,
                            Quaternion.Euler(0, 0, 90));
                        break;
                    default:
                        obstacle = Instantiate(obstacles[obstacleIndex], targetProperties.position + agregate,
                            Quaternion.identity);
                        break;
                }
                currentTimeToInstantiate = waitTimeToInstantiate;
                objectCount += 1;
            }
        }
        if (obstaclesCount >= obstaclesAmount)
        {
            playing = false;

            StartCoroutine(wait());
        }
    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(3f);
        progressBar.SetActive(false);
        this.gameObject.SetActive(false);
        timer.resetTimer();
        finalMenu.SetActive(true);
    }
    void InstantiateHeatingObstacles()
    {
        if (selectTargetController.canMove && currentTime < limitTime)
        {
            currentTimeToInstantiate -= 1 * Time.deltaTime;
            if (currentTimeToInstantiate <= 0)
            {
                obstaclesCount++;
                switch (obstacleIndex)
                {
                    case 1:
                        if (cylinderCount < 1)
                        {
                            tutorial.SetActive(true);
                            timer.timeScale = 1;
                            textTutorial.text = "Agachate para evitar el cilindro";
                            cylinderCount++;
                        }
                        obstacle = Instantiate(obstacles[obstacleIndex], targetProperties.position + agregate,
                            Quaternion.Euler(0, 0, 90));
                        
                        break;
                    default:
                        obstacle = Instantiate(obstacles[obstacleIndex], targetProperties.position + agregate,
                            Quaternion.identity);
                        if (latCount < 2)
                        {
                            tutorial.SetActive(true);
                            textTutorial.text = "Muevete a los lados para evitar los cubos y esferas";
                            latCount++;
                        }
                        break;
                }
                currentTimeToInstantiate = waitTimeToInstantiate;
                objectCount +=1;
                if (obstaclesCount>2)
                {
                    tutorial.SetActive(false);
                }
            }
        }
        if(obstaclesCount >= obstaclesAmount)
        {
            generate = false;
            playing = false;
            StartCoroutine(wait());
        }
    }
}
