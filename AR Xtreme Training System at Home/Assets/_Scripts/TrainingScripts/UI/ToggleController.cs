﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ToggleController : MonoBehaviour
{
    [SerializeField] private Toggle cube; 
    [SerializeField] private Toggle cylinder; 
    [SerializeField] private Toggle sphere; 
    
    [SerializeField] private TraningController trainingController;
    private GameObject[] customList;
    [SerializeField] private GameObject cubeObj;
    [SerializeField] private GameObject cylinderObj;
    [SerializeField] private GameObject sphereObj;

    public bool clickedCube;
    public bool clickedSphere;
    public bool clickedCylinder;
    // Start is called before the first frame update
    void Start()
    {
        cube.isOn = false;
        cylinder.isOn = false;
        sphere.isOn = false;
    }

    // Update is called once per frame
    void Update()
    {
        check();
    }

    void check()
    {
        //Cube
        if (cube.isOn)
        {
            clickedCube = true;
        }
        if (!cube.isOn)
        {
            clickedCube = false;
        }
        //Cylinder
        if (cylinder.isOn)
        {
            clickedCylinder = true;
        }
        if (!cylinder.isOn)
        {
            clickedCylinder = false;
        }
        //Sphere
        if (sphere.isOn)
        {
            clickedSphere = true;
        }
        if (!sphere.isOn)
        {
            clickedSphere = false;
        }
    }
}
