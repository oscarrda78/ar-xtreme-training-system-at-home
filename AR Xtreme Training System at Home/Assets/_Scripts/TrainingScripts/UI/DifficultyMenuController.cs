﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class DifficultyMenuController : MonoBehaviour
{
    [Header("Paneles")]
    [SerializeField] private GameObject difficultyPanel;
    [SerializeField] private Button back;
    [SerializeField] private GameObject customMenu;

    //[SerializeField] private GameObject countDownPanel;
    [SerializeField] private GameObject selectTargetPanel;
    [SerializeField] private GameObject progressBar;
    [SerializeField] private GameObject scoreBar;
    [SerializeField] private GameObject trainingObjController;
    [SerializeField] private GameObject objectObjGenerate;
    [SerializeField] private GameObject instructions;
    private IntructionsController intructionsController;
    private TraningController traningController;
    private ObjectGenerate objectGenerate;
    
    public float startTimeToInstantiate;
    public float waitTimeToInstantiate;
    
    

    // Start is called before the first frame update
    void Start()
    {
        trainingObjController.SetActive(false);
        traningController = trainingObjController.GetComponent<TraningController>();
        objectGenerate = objectObjGenerate.GetComponent<ObjectGenerate>();
        intructionsController = instructions.GetComponent<IntructionsController>();
        objectObjGenerate.SetActive(false);
        customMenu.SetActive(false);
        difficultyPanel.SetActive(true);
        selectTargetPanel.SetActive(false);
        progressBar.SetActive(false);
        scoreBar.SetActive(false);
        back.onClick.AddListener(delegate { GoToScene("Menu"); });
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SelectDifficulty(string text)
    {
        difficultyPanel.SetActive(false);
        //selectTargetPanel.SetActive(true);
        //progressBar.SetActive(false);
        //scoreBar.SetActive(false);
        switch (text){
            case "easy":
                intructionsController.file = text;
                intructionsController.notIsCustom = true;
                //objectObjGenerate.SetActive(true);
                instructions.SetActive(true);
                break;  
            case "intermediate":
                intructionsController.file = text;
                intructionsController.notIsCustom = true;
                objectObjGenerate.SetActive(true);
                instructions.SetActive(true);
                break;
            case "advanced":
                intructionsController.file = text;
                intructionsController.notIsCustom = true;
                objectObjGenerate.SetActive(true);
                instructions.SetActive(true);
                break;
            case "custom":
                customMenu.SetActive(true);
                objectObjGenerate.SetActive(false);
                trainingObjController.SetActive(false);
                instructions.SetActive(false);
                selectTargetPanel.SetActive(false);
                back.gameObject.SetActive(false);
                difficultyPanel.SetActive(false);
                break;
            default:
                startTimeToInstantiate = 1f;
                waitTimeToInstantiate = 5f;
                break;
        }
    }
    void GoToScene(string otherScene)
    {
        SceneManager.LoadScene(otherScene);
    }

}
