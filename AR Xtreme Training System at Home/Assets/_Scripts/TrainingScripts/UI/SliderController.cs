﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderController : MonoBehaviour
{
    [SerializeField] private TraningController trainingController;

    private float limitTime = 0f;
    private int obstaclesAmount = 0;

    private bool isTime;
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.CompareTag("TimeOptions"))
        {
            isTime = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isTime)
        {
            trainingController.setLimitTime(limitTime);
        }
        else
        {
            trainingController.setObstaclesAmount(obstaclesAmount);
        }
    }

    public void OnValueChanged(float value)
    {
        if (isTime)
        {
            limitTime = value;
            Debug.Log("Tiempo: " + trainingController.limitTime);
        }
        else
        {
            obstaclesAmount = (int)value;
            Debug.Log("Obstaculos: " + trainingController.obstaclesAmount);
        }
    }
}
