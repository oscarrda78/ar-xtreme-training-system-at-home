﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HeatTrainenu : MonoBehaviour
{
    [SerializeField] private Button back;
    // Start is called before the first frame update
    void Start()
    {
        back.onClick.AddListener(delegate { GoToScene("Menu"); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void GoToScene(string otherScene)
    {
        SceneManager.LoadScene(otherScene);
    }
}
