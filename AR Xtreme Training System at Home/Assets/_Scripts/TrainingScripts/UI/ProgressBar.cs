﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ProgressBar : MonoBehaviour
{

    [SerializeField] private float maximum;
    private float minimum = 0f;
    private float fillAmount = 0f;
    private float currentOffSet = 0f;
    private float maximumOffSet = 0f;
    private bool playing = true;
    [SerializeField] private Text progressText;

    [SerializeField] private TraningController traningController;
    [SerializeField] private GameObject finalMenu;
    [SerializeField] private Image mask;
    
    // Start is called before the first frame update
    void Start()
    {
        finalMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (playing)
        {
            maximum = traningController.obstaclesAmount;
            GetCurrentFill(traningController.obstaclesCount);
        }
    }

    void GetCurrentFill(int obstaclesCurrent)
    {
        currentOffSet = (float)obstaclesCurrent - minimum;
        maximumOffSet = (float)maximum - minimum;
        fillAmount = currentOffSet / maximumOffSet;
        progressText.text = (fillAmount*100).ToString();
        mask.fillAmount = fillAmount;
        if (fillAmount >= 1)
        {
            fillAmount = 1;
            StartCoroutine(wait());
        }
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(3f);
        this.gameObject.SetActive(false);
        finalMenu.SetActive(true);
    }

    public void ResetProgress()
    { 
        minimum = 0f;
        fillAmount = 0f;
        currentOffSet = 0f;
        maximumOffSet = 0f;
    }
}
