﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class IntructionsController : MonoBehaviour
{
    [SerializeField] private GameObject trainingObjController;
    [SerializeField] private GameObject objectObjGenerate;
    [SerializeField] private GameObject selectTargetPanel;
    [SerializeField] private GameObject countDownPanel;
    [SerializeField] private GameObject instructionsPanel;
    [SerializeField] private GameObject target;
    [SerializeField] private Text name;
    [SerializeField] private Text description;
    [SerializeField] private RawImage video;
    public Texture[] texturas;
    private TraningController traningController;
    public string file;
    public bool notIsCustom = false;
    public string type;
    [SerializeField] private Button startButton;
    private ObjectGenerate objectGenerate;
    private bool first = false;

    private void OnEnable()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        startButton.onClick.AddListener(delegate { GoOutInstructions(); });
        trainingObjController.SetActive(false);
        objectGenerate = objectObjGenerate.GetComponent<ObjectGenerate>();
        traningController = trainingObjController.GetComponent<TraningController>();
        if (notIsCustom)
        {
            objectGenerate.Locate(file);
        }
        CreateObjects();
    }

    // Update is called once per frame
    void Update()
    {
        name.text = objectGenerate.GetSessionName();
        description.text = objectGenerate.GetDescription();
        video.texture = texturas[objectGenerate.GetVideo()];
    }

    private void GoOutInstructions()
    {
        traningController.first = true;
        traningController.playing = true;
        traningController.generate = true;
        traningController.score = 0;
        traningController.dodgedObjects = 0;
        traningController.hitObjects = 0;
        trainingObjController.SetActive(true);
        selectTargetPanel.SetActive(true);
        countDownPanel.SetActive(true);
        instructionsPanel.SetActive(false);
        this.gameObject.SetActive(false);
        TrackerManager.Instance.GetTracker<ObjectTracker>().Start();

    }

    private void CreateObjects()
    {
        objectGenerate.CreateObstacles(objectGenerate.NextObstacleList());
    }
}
