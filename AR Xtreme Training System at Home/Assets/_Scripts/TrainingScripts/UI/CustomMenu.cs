﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomMenu : MonoBehaviour
{
    [SerializeField] private GameObject difficultyPanel;
    [SerializeField] private GameObject options; 
    [SerializeField] private Button startButton;
    [SerializeField] private Button eraseButton;
    [SerializeField] private Button backOptionsButton;
    [SerializeField] private GameObject traningObjController;
    [SerializeField] private GameObject instructions;
    private IntructionsController intructionsController;
    private TraningController traningController;
    [SerializeField] private GameObject objectObjGenerate;
    private ObjectGenerate objectGenerate;
    [Header("Buttons")]
    [SerializeField] private Toggle ex1Button;
    [SerializeField] private Toggle ex2Button;
    [SerializeField] private Toggle ex3Button;
    [SerializeField] private Toggle ex4Button;
    [SerializeField] private Toggle ex5Button;
    [SerializeField] private Toggle ex6Button;
    [SerializeField] private Toggle ex7Button;
    [SerializeField] private Toggle ex8Button;
    [SerializeField] private Toggle ex9Button;
    [SerializeField] private Toggle ex10Button;
    private ObstacleList obstaculos;

    private bool validateEx1;
    private bool validateEx2 ;
    private bool validateEx3 ;
    private bool validateEx4;
    private bool validateEx5;
    private bool validateEx6;
    private bool validateEx7;
    private bool validateEx8;
    private bool validateEx9;
    private bool validateEx10;

    public string file_name;
    
    // Start is called before the first frame update
    void Start()
    {
        instructions.SetActive(false);
        traningObjController.SetActive(false);
        traningController = traningObjController.GetComponent<TraningController>();
        intructionsController = instructions.GetComponent<IntructionsController>();
        objectGenerate = objectObjGenerate.GetComponent<ObjectGenerate>();
        objectObjGenerate.SetActive(true);
        buttonActions();
    }

    // Update is called once per frame
    void Update()
    {
        CheckToggle();
    }

    void buttonActions()
    {
        backOptionsButton.onClick.AddListener(delegate { GoBackDifficultyPanel(); });
        startButton.onClick.AddListener(delegate { GoOutPanel(); });
        eraseButton.onClick.AddListener(delegate { EraseSelection(); });
    }

    private void EraseSelection()
    {
        ex1Button.isOn = false;
        ex2Button.isOn = false;
        ex3Button.isOn = false;
        ex4Button.isOn = false;
        ex5Button.isOn = false;
        ex6Button.isOn = false;
        ex7Button.isOn = false;
        ex8Button.isOn = false;
        ex9Button.isOn = false;
        ex10Button.isOn = false;
        objectGenerate.Clean();
    }
    private void GoOutPanel()
    {
        intructionsController.notIsCustom = false;
        instructions.SetActive(true);
        this.gameObject.SetActive(false);
    }

    private void GoBackDifficultyPanel()
    {
        difficultyPanel.SetActive(true);
        this.gameObject.SetActive(false);
    }

    private void CheckToggle()
    {
        //Ex1 Button
        if (ex1Button.isOn && !validateEx1)
        {
            objectGenerate.Add("ex1");
            validateEx1 = true;
        }
        if (!ex1Button.isOn)
        {
            validateEx1 = false;
        }
        //Ex2 Button
        if (ex2Button.isOn && !validateEx2)
        {
            objectGenerate.Add("ex2");
            validateEx2 = true;
        }
        if (!ex2Button.isOn)
        {
            validateEx2 = false;
        }
        //Ex3 Button
        if (ex3Button.isOn && !validateEx3)
        {
            objectGenerate.Add("ex3");
            validateEx3 = true;
        }
        if (!ex3Button.isOn)
        {
            validateEx3 = false;
        }
        //Ex4 Button
        if (ex4Button.isOn && !validateEx4)
        {
            objectGenerate.Add("ex4");
            validateEx4 = true;
        }
        if (!ex4Button.isOn)
        {
            validateEx4 = false;
        }
        //Ex5 Button
        if (ex5Button.isOn && !validateEx5)
        {
            objectGenerate.Add("ex5");
            validateEx5= true;
        }
        if (!ex5Button.isOn)
        {
            validateEx5 = false;
        }
        //Ex6 Button
        if (ex6Button.isOn && !validateEx6)
        {
            objectGenerate.Add("ex6");
            validateEx6= true;
        }
        if (!ex6Button.isOn)
        {
            validateEx6 = false;
        }
        //Ex7 Button
        if (ex7Button.isOn && !validateEx7)
        {
            objectGenerate.Add("ex7");
            validateEx7= true;
        }
        if (!ex7Button.isOn)
        {
            validateEx7 = false;
        }
        //Ex8 Button
        if (ex8Button.isOn && !validateEx8)
        {
            objectGenerate.Add("ex8");
            validateEx8= true;
        }
        if (!ex8Button.isOn)
        {
            validateEx8 = false;
        }
        //Ex9 Button
        if (ex9Button.isOn && !validateEx9)
        {
            objectGenerate.Add("ex9");
            validateEx9= true;
        }
        if (!ex9Button.isOn)
        {
            validateEx9 = false;
        }
        //Ex10 Button
        if (ex10Button.isOn && !validateEx10)
        {
            objectGenerate.Add("ex10");
            validateEx10= true;
        }
        if (!ex10Button.isOn)
        {
            validateEx10 = false;
        }
    }
    
}
