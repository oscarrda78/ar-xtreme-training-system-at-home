﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CameraBackground : MonoBehaviour
{
    private RawImage imagen;
    private WebCamTexture camTexture;


    // Start is called before the first frame update
    void Start()
    {
        imagen = GetComponent<RawImage>();
        camTexture = new WebCamTexture(Screen.width,Screen.height);
        imagen.texture = camTexture;
        camTexture.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if(camTexture.width < 100){
            return;
        }
        float cwNeeded = -camTexture.videoRotationAngle;
        if(camTexture.videoVerticallyMirrored){
            cwNeeded +=180f;
        }
        imagen.rectTransform.localEulerAngles = new Vector3(0f,0f,cwNeeded);

        float videoRatio = (float)camTexture.width /(float)camTexture.height;

    }
}
