﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [Tooltip("Initial Time in seconds")]
    public int initialTime;

    [Tooltip("Time scale")]
    [Range(-10.0f,10.0f)]
    [SerializeField] 
    public float timeScale = 0;
    [SerializeField] private GameObject finalMenu;
    [SerializeField] private GameObject progressbar;
    public float limitTime = 120f;
    
    private Text myText;
    private float frameTimeScale = 0f;
    public float timeinSecondsToShow = 0f;
    private float pauseTimeScale, initialTimeScale;
    private bool isPaused = false;
    
    // Start is called before the first frame update
    void Start()
    {
        finalMenu.SetActive(false);
        initialTimeScale = timeScale;

        myText = GetComponent<Text>();
        
        timeinSecondsToShow = initialTime;

        updateTime(initialTime);

    }

    // Update is called once per frame
    void Update()
    {
        frameTimeScale = Time.deltaTime * timeScale;

        timeinSecondsToShow += frameTimeScale;
        updateTime(timeinSecondsToShow);
    }

    public void updateTime(float timeInSeconds)
    {
        int minutes = 0;
        int seconds = 0;
        string textClock;

        if (timeInSeconds < 0) timeInSeconds = 0;

        //if (timeInSeconds >= limitTime)
        //{
        //    timeScale = 0;
        //    StartCoroutine(wait());
        //}

        minutes = (int) timeInSeconds / 60;
        seconds = (int) timeInSeconds % 60;

        textClock = minutes.ToString("00") + ":" + seconds.ToString("00");

        myText.text = textClock;

    }
    
    IEnumerator wait()
    {
        yield return new WaitForSeconds(3f);
        progressbar.SetActive(false);
        this.gameObject.SetActive(false);
        resetTimer();
        finalMenu.SetActive(true);
    }

    public void resetTimer()
    {
        timeinSecondsToShow = 0f;
        frameTimeScale = 0f;
        timeScale = 1;
    }
    
}
