﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Vuforia;

public class SelectTargetController : DefaultTrackableEventHandler
{
    [SerializeField] private GameObject countDownPanel;
    [SerializeField] private GameObject selectTargetPanel;
    [SerializeField] private Text countDownText;
    [SerializeField] private GameObject progressBar;
    [SerializeField] private GameObject scoreBar;
    [System.NonSerialized] public Transform targetSelected;
    [SerializeField] private GameObject target;

    public bool isCountDown;
    public bool canMove;
    private float currentTime = 0f;
    private float startTime = 3f;
    [SerializeField] private bool isSelectTarget;
    [SerializeField] private GameObject visor;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        currentTime = startTime;
        countDownPanel.SetActive(false);
        progressBar.SetActive(false);
        scoreBar.SetActive(false);
        visor.GetComponent<Renderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isCountDown)
        {
            CountDown();
        }
    }

    private void CountDown()
    {
        currentTime -= 1 * Time.deltaTime;
        countDownText.text = currentTime.ToString("0");
        if (currentTime <= 0)
        {
            currentTime = 0;
            countDownText.text = "GO!";
            StartCoroutine(HidePanel());
        }
    }
    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        targetSelected = target.transform;
        isCountDown = true;
        countDownPanel.SetActive(true);
        selectTargetPanel.SetActive(false);
        TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
    }
    public void SelectTarget(){

    }

    IEnumerator HidePanel(){
        yield return new WaitForSeconds(1);
        countDownPanel.SetActive(false);
        visor.GetComponent<Renderer>().enabled = true;
        canMove = true;
    }
    
}
