﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class ObjectGenerate : MonoBehaviour
{
    private string file = "customGame.txt";
    public string sessionName;
    public string description;
    public int video;
    public TextAsset[] files;

    public List<string> exercises = new List<string>();
    public string type;

    public ObstacleList obstacleList;
    public List<ObstacleList> obstacleLists;
    private List<Obstacle> obstacles = new List<Obstacle>();
    public ObstacleList obstacleCustomList;
    private bool first = true;
    public bool lastList = false;
    private bool change = true;
    public int cont = 0;
    // Start is called before the first frame update
    void Start()
    {
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }

    public int NextObstacleList()
    {
        if (first)
        {
            cont = 0;
            first = false;
        }
        else
        {
            if (cont < obstacleLists.Count)
            {
                cont++;
            }
        }
        if (cont + 1 >= obstacleLists.Count && change)
        {
            lastList = true;
            change = false;
        }
        
        return cont;    
    }

    public int get_file_title(string file)
    {
        switch (file)
        {
            case "heating":
                Heating();
                return 10;
            case "easy":
                Beginner();
                return 11;
            case "intermediate":
                Intermediate();
                return 12;
            case "advanced":
                Advanced();
                return 13;
            case "ex1":
                return 0;
            case "ex2":
                return 1;
            case "ex3":
                return 2;
            case "ex4":
                return 3;
            case "ex5":
                return 4;
            case "ex6":
                return 5;
            case "ex7":
                return 6;
            case "ex8":
                return 7;
            case "ex9":
                return 8;
            case "ex10":
                return 9;
            default:
                return 10;
        }
    }

    public void Locate(string file)
    {
        int i = get_file_title(file);
    }
    public void Add(string file)
    {
        int i = get_file_title(file);
        ObstacleList obstacleListRead = JsonUtility.FromJson<ObstacleList>(files[i].text);
        obstacleLists.Add(obstacleListRead);
        type = "Custom";
        exercises.Add(obstacleListRead.name);
    }

    public void Heating()
    {
        for (int i = 0; i < 3; i++)
        {
            ObstacleList obstacleListH = JsonUtility.FromJson<ObstacleList>(files[i].text);
            obstacleLists.Add(obstacleListH);
        }

        type = "Heating";
    }
    
    public void Beginner()
    {
        int i;
        for (i = 3; i < 6; i++)
        {
            ObstacleList obstacleListH = JsonUtility.FromJson<ObstacleList>(files[i].text);
            obstacleLists.Add(obstacleListH);
            exercises.Add(obstacleListH.name);
        }

        this.type = "Beginner";
    }
    
    public void Intermediate()
    {
        ObstacleList obstacleListH = JsonUtility.FromJson<ObstacleList>(files[3].text);
        obstacleLists.Add(obstacleListH);
        exercises.Add(obstacleListH.name);
        obstacleListH = JsonUtility.FromJson<ObstacleList>(files[6].text);
        obstacleLists.Add(obstacleListH);
        exercises.Add(obstacleListH.name);
        obstacleListH = JsonUtility.FromJson<ObstacleList>(files[7].text);
        obstacleLists.Add(obstacleListH);
        exercises.Add(obstacleListH.name);
        this.type = "Intermediate";
    }
    
    public void Advanced()
    {
        ObstacleList obstacleListH = JsonUtility.FromJson<ObstacleList>(files[3].text);
        obstacleLists.Add(obstacleListH);
        exercises.Add(obstacleListH.name);
        obstacleListH = JsonUtility.FromJson<ObstacleList>(files[8].text);
        obstacleLists.Add(obstacleListH);
        exercises.Add(obstacleListH.name);
        obstacleListH = JsonUtility.FromJson<ObstacleList>(files[9].text);
        obstacleLists.Add(obstacleListH);
        exercises.Add(obstacleListH.name);

        this.type = "Advanced";
    }

    public void CreateObstacles(int n)
    {
        ObstacleList[] obstacleListsAux = obstacleLists.ToArray();
        ObstacleList next = obstacleListsAux[n];
        sessionName = next.name;
        description = next.description;
        video = next.video;
        Obstacle[] obstacle = next.obstacle;
        int repeat = next.repeat;
        for(int i = 0; i < repeat; i++)
        {
            for(int j = 0; j < obstacle.Length; j++)
            {
                Obstacle aux = new Obstacle(obstacle[j].type, obstacle[j].x, obstacle[j].y, obstacle[j].z);
                obstacles.Add(aux);
            }
        }
        Save();
    }

    public string GetSessionName()
    {
        return sessionName;
    }

    public int GetVideo()
    {
        return video;
    }
    
    public string GetDescription()
    {
        return description;
    }
    
    public void Save()
    {
        obstacleCustomList = new ObstacleList(obstacleLists[cont].name,obstacleLists[cont].description,obstacleLists[cont].video,obstacleLists[cont].repeat,obstacles.ToArray());
        string json = JsonUtility.ToJson(obstacleCustomList);
        WriteToFile(file, json);
    }
    
    public void WriteToFile(string _fileName, string _json)
    {
        string path = GetFilePath(_fileName);
        FileStream fileStream = new FileStream(path, FileMode.Create);
        using (StreamWriter writer = new StreamWriter(fileStream))
        {
            writer.Write(_json);
        }
    }

    public void Load()
    {
        string json = ReadFromFile(file);
        JsonUtility.FromJsonOverwrite(json, obstacleList);
    }
    
    public string ReadFromFile(string _fileName)
    {
        string path = GetFilePath(_fileName);

        if (File.Exists(path))
        {
            using (StreamReader reader = new StreamReader(path))
            {
                string json = reader.ReadToEnd();
                return json;
            }
        }
        else
            print("File Not Found");
        return "";
    }

    public void Clean()
    {
        obstacles = new List<Obstacle>();
        obstacleCustomList = new ObstacleList("","",0,0,obstacles.ToArray());
    }
    
    public string GetFilePath(string fileName)
    {
        return Application.persistentDataPath + "/" + fileName;
    }

}
