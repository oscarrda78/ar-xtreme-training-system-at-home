﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AudioManager : MonoBehaviour
{

    private int firstPlayInt;
    [SerializeField] private Slider musicVolumenSlider,sfxVolumenSlider;
    private float musicVolumenValue = 0.60f;
    private float sfxVolumenValue = 0.60f;
    public AudioSource musicAudio;
    public AudioSource[] sfxAudio;

    void Start()
    {
        musicVolumenSlider.value = musicVolumenValue;
        sfxVolumenSlider.value = sfxVolumenValue;
    }

    public void UpdateSound()
    {
        musicAudio.volume = musicVolumenSlider.value;
        for (int i = 0; i < sfxAudio.Length; i++)
        {
            sfxAudio[i].volume = sfxVolumenSlider.value;
        }
    }
}
