﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class UserData
{
    public string nameuser = "Nombre";
    public string size = "0";
    public string weight = "0.0";
    public string age = "0";
    public string sex = "Sexo";
}
