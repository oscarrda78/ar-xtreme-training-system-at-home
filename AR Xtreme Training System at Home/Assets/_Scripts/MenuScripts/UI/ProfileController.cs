﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileController : MonoBehaviour
{
    [Header("Profile")]
    [SerializeField] private GameObject editProfilePanel;
    [SerializeField] private Button backEditProfilebtn;
    [SerializeField] private Button editProfilebtn;
    [SerializeField] private Button saveProfilebtn;
    [SerializeField] private List<GameObject> UserInput;
    [SerializeField] private List<GameObject> UserData;
    private AudioSource[] sfxAudio;
    public DataManager dataManager;

    private void Awake()    
    {
        LoadData();
    }
    void Start()
    {
        sfxAudio = GameObject.FindGameObjectWithTag("music").GetComponent<AudioManager>().sfxAudio;
        editProfilePanel.SetActive(false);
        SetButtons();
    }
    void LoadData(){
        dataManager.Load();
        UserData[0].GetComponent<Text>().text = dataManager.data.nameuser;
        UserData[1].GetComponent<Text>().text = dataManager.data.size;
        UserData[2].GetComponent<Text>().text = dataManager.data.weight;
        UserData[3].GetComponent<Text>().text = dataManager.data.age;
        UserData[4].GetComponent<Text>().text = dataManager.data.sex;

        ShowUserInputData();
    }
    void ShowUserInputData(){
        UserInput[0].GetComponent<InputField>().text = dataManager.data.nameuser;
        UserInput[1].GetComponent<InputField>().text = dataManager.data.size;
        UserInput[2].GetComponent<InputField>().text = dataManager.data.weight;
        UserInput[3].GetComponent<InputField>().text = dataManager.data.age;
    }
    void SaveData(){
        dataManager.data.nameuser = UserInput[0].GetComponent<InputField>().text;
        dataManager.data.size = UserInput[1].GetComponent<InputField>().text;
        dataManager.data.weight = UserInput[2].GetComponent<InputField>().text;
        dataManager.data.age = UserInput[3].GetComponent<InputField>().text;

        var dropdown = UserInput[4].GetComponent<Dropdown>();
        dataManager.data.sex = dropdown.options[dropdown.value].text;

        dataManager.Save();
        LoadData();
    }
    void SetButtons()
    {
        backEditProfilebtn.onClick.AddListener(delegate { GetOutPanel(editProfilePanel); });
        editProfilebtn.onClick.AddListener(delegate { GoToPanel(editProfilePanel); });
        saveProfilebtn.onClick.AddListener(delegate { GoToSaveProfile(); });
    }
    void GoToPanel(GameObject otherPanel)
    {
        sfxAudio[0].Play();
        otherPanel.SetActive(true);
    }
    void GetOutPanel(GameObject otherPanel)
    {
        sfxAudio[0].Play();
        otherPanel.SetActive(false);
    }
    void GoToSaveProfile()
    {
        sfxAudio[0].Play();
        GetOutPanel(editProfilePanel);
        SaveData();
    }

    private UserData createSaveGameObject(){
        UserData user = new UserData();
        user.nameuser = UserInput[0].GetComponent<InputField>().text;
        user.nameuser = UserInput[1].GetComponent<InputField>().text;
        user.nameuser = UserInput[2].GetComponent<InputField>().text;
        user.nameuser = UserInput[3].GetComponent<InputField>().text;
        
        var dropdown = UserInput[4].GetComponent<Dropdown>();
        user.nameuser = dropdown.options[dropdown.value].text;
        
        return user;
    }

}
