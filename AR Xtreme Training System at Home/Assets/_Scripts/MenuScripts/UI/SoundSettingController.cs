﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SoundSettingController : MonoBehaviour
{
    [SerializeField] private Button backSoundSettingbtn;
    private AudioSource[] sfxAudio;
    void Start()
    {
        sfxAudio = GameObject.FindGameObjectWithTag("music").GetComponent<AudioManager>().sfxAudio;
        SetButtons();
    }
    void SetButtons()
    {
        backSoundSettingbtn.onClick.AddListener(delegate { GetOutPanel(); });
    }
    void GetOutPanel()
    {
        sfxAudio[0].Play();
        this.gameObject.SetActive(false);
    }
}
