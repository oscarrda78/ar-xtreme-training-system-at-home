﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [Header("Botones del Menu Principal")]
    [SerializeField] private Button initBtn;
    [SerializeField] private Button configurationBtn;
    [SerializeField] private Button recordBtn;
    [SerializeField] private Button heatingBtn;

    [Header("Botones de Configuracion")]
    [SerializeField] private Button profileBtn;
    [SerializeField] private Button controlsSettingBtn;
    [SerializeField] private Button soundSettingBtn;
    [SerializeField] private Button backConfigurationBtn;

    [Header("Botones de Perfil")]
    [SerializeField] private Button backProfileBtn;

    [Header("Paneles")]
    [SerializeField] private GameObject configurationPanel;
    [SerializeField] private GameObject profilePanel;
    [SerializeField] private GameObject soundSettingPanel;

    [Header("Nombre de archivo background")]
    [SerializeField] private string nameBackgroundImg;

    [SerializeField] private GameObject imagenBackground;


    private AudioSource[] sfxAudio;

    // Start is called before the first frame update
    void Start()
    {  
        sfxAudio = GameObject.FindGameObjectWithTag("music").GetComponent<AudioManager>().sfxAudio;

        SetImageBackground();
        SetButtons();
        InitPanels();
    }
    void SetImageBackground()
    {
        Image imgBackground = imagenBackground.GetComponent<Image>();
        Sprite newImage = Resources.Load<Sprite>("Sprites/"+nameBackgroundImg);
        imgBackground.sprite = newImage;
    }
    void InitPanels(){
        configurationPanel.SetActive(false);
        profilePanel.SetActive(false);
        soundSettingPanel.SetActive(false);
    }
    void SetButtons()
    {
        heatingBtn.onClick.AddListener(delegate { GoToScene("Heating"); });
        initBtn.onClick.AddListener(delegate { GoToScene("Training"); });
        recordBtn.onClick.AddListener(delegate { GoToScene("History"); });

        configurationBtn.onClick.AddListener(delegate { GoToPanel(configurationPanel); });
        profileBtn.onClick.AddListener(delegate { GoToPanel(profilePanel); });
        soundSettingBtn.onClick.AddListener(delegate { GoToPanel(soundSettingPanel); });

        backConfigurationBtn.onClick.AddListener(delegate { GetOutPanel(configurationPanel); });
        backProfileBtn.onClick.AddListener(delegate { GetOutPanel(profilePanel); });
        
    }
    void GoToScene(string otherScene)
    {
        sfxAudio[1].Play();
        SceneManager.LoadScene(otherScene);
    }
    void GoToPanel(GameObject otherPanel)
    {
        sfxAudio[0].Play();
        otherPanel.SetActive(true);
    }
    void GetOutPanel(GameObject otherPanel)
    {
        sfxAudio[0].Play();
        otherPanel.SetActive(false);
    }
}
