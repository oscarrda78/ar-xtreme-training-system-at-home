{
    "id":7,
    "name": "MB/DB lateral reaching",
    "description":"Párese con los pies separados a la altura de las caderas y las rodillas dobladas. Sostenga una pelota medicinal o mancuernas en sus manos. Manteniendo el tronco apretado, da un gran paso lateral con el pie izquierdo, doblando ligeramente la pierna izquierda. Flexione las caderas y alcance el pie izquierdo hasta que sienta un estiramiento cómodo en los isquiotibiales izquierdos. Regrese a la posición inicial y repita hacia el lado derecho. ", 
    "video":6,
    "repeat":2,
    "obstacle": [
        {   
            "type": 1,
            "x": 0.0,
            "y": 0.0,
            "z": 0.0
        },
        {
            "type": 1,
            "x": 0.0,
            "y": 0.0, 
            "z": 0.0
        }
    ]
}